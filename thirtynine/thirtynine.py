#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/games
## a component of eRis.Cogs
* Provides miscellaneous fun commands for users
* Provides Warframe API interface for users
"""

# Standard Library Imports #
import datetime
import io
import json
import random

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog
from eris.cogs.libCore import libCore
from eris.cogs.libConfig import libConfig
from eris.logger import logger


class thirtynine(eris.cog.Cog):
    """The Game of 39"""

    def __init__(self, **kwargs):
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.cogs.THiRTYNiNE")
        self.core: libCore = kwargs["deps"]["libCore"]
        self.config: libConfig = kwargs["deps"]["libConfig"]
