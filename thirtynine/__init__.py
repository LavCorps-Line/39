#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /thirtynine
## a component of THiRTYNiNE
* Provides the game of 39 for users
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .thirtynine import thirtynine


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=thirtynine(**kwargs))
